package com.yss.cms.controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.yss.cms.entity.ClientInfoEntity;
import com.yss.cms.entity.UserEntity;
import com.yss.cms.entity.UserRoleEntity;
import com.yss.cms.service.IGenericService;
import com.yss.cms.utility.AES;

@Controller
public class UserController {
	
	private static final Logger logger = LoggerFactory.getLogger(UserController.class);
	
	
	@Value("${customerid.prifix}")
	private String customerIdPrifix;
	
	@Autowired
	IGenericService<UserEntity> userServices;
	@Autowired
	IGenericService<UserRoleEntity> userRoleServices;
	@Autowired
	private IGenericService<ClientInfoEntity> clientInfoService;
	
	
	
	@RequestMapping("/userlist")
	public String userList(Model model) {
		List<UserEntity> userList=userServices.findAll(new UserEntity());
		List<UserRoleEntity> roleList=userRoleServices.findAll(new UserRoleEntity());
		model.addAttribute("list",userList);
		model.addAttribute("roles",roleList);
		return "user_profile";
	}

	@RequestMapping("/login")
	public String getLogin(Model model,@ModelAttribute("user") UserEntity user,HttpSession session) {
	logger.info("\n --------------------------------\n loing user : "+user.getUserName()+"  password  : "+user.getPassword()+"\n  ----------------------------------");
	
	String pageName="homepage";
	try {
		if(user!=null) {
			String query="WHERE userName='"+user.getUserName()+"' AND password ='"+AES.encrypt(user.getPassword())+"'";
			UserEntity entity=userServices.findAll(new UserEntity(),query).get(0);
			
			System.out.println(" -------entity--------------- "+entity);
		if(entity !=null) {
			model.addAttribute("error_message","");
			 session.setAttribute("user", entity);
			//pageName="homepage";
			 String pp="index";
			 logger.info("--------------\n USER ROLE IS :"+entity.getRole()+"   \n----------------------");
			 if(entity.getRole().getRoleType().equals("USER")) {
				
				 pp="product";
			 }else {
				 pp="home";
			 }
			 return "redirect:"+pp;
		}else {
			logger.info("--------------\n USER DETAILS NOT IN DATABASE   \n----------------------");
			model.addAttribute("error_message","This User not here");
			pageName="redirecat:/signin";
		}
				
		}else {
			logger.info("--------------\n USER DETAILS NULL   \n----------------------");	
		}
		
		return pageName;	
	}catch(NullPointerException exp) {
		exp.getMessage();
		logger.info("------\n  NULL POINTER EXCEPTION  HERE "+exp.getMessage()+"\n");
		model.addAttribute("error_message","something went wrong");
		//return "redirect:/";
		return "index";
	}catch(Exception exp) {
		exp.getMessage();
		logger.info("------\n  SOME EXCEPTION  HERE "+exp.getMessage()+"\n");
		model.addAttribute("error_message","Not a Valid User");
		//return "redirect:/";
		return "index";
	}
	
	
		 
	}
	
	
	
	@RequestMapping("/home")
	public String goToHomePage(Model model,HttpSession session) {
		UserEntity entity=(UserEntity) session.getAttribute("user");
		System.out.println("session is "+entity);
		
		if(entity==null) {
			return "redirect:/signin";
		}else if(entity.getRole().getRoleType().equals("USER")) {
			model.addAttribute("role", entity.getRole().getRoleType());
			return "product_list";
		}else if(entity.getRole().getRoleType().equals("DELIVERY MAN")) {
			model.addAttribute("role", entity.getRole().getRoleType());
			return "defuser_delivery";
		}else if(entity.getRole().getRoleType().equals("ADMIN") || entity.getRole().getRoleType().equals("SUPER ADMIN")) {
			model.addAttribute("role", entity.getRole().getRoleType());
			return "homepage";
		}else {
			return "redirect:/signin";
		}
		
		
	}
	
	
	@RequestMapping(value="/registration", method=RequestMethod.POST)
	public String customerRegistrationInfo(Model model,@ModelAttribute ("clientInfo") ClientInfoEntity clientInfo) throws Exception {
		
		System.out.println("----------sdfffffffffffffffffff-------' "+clientInfo);
		UserRoleEntity role=null;
		List<UserEntity> list=userServices.findAll(new UserEntity());
		if(list.size()<=0) {
			role=userRoleServices.findOne(new UserRoleEntity(), 1);
		}else {
			role=userRoleServices.findOne(new UserRoleEntity(), 3);
		}
		
		
		clientInfoService.save(clientInfo);
		long id=clientInfo.getId();
		clientInfo.setCustomerId("yss"+id);
		ClientInfoEntity client =clientInfoService.update(clientInfo);
		System.out.println("-----------------' "+client);
		UserEntity user=new UserEntity();
		user.setName(client.getCompanyName());
		user.setMobile(client.getContactNo());
		user.setPassword(AES.encrypt(client.getContactNo()));
		
		System.out.println("role "+role);
		user.setRole(role);
		user.setUserName(client.getCustomerId());
		userServices.save(user);
		/*user.setPassword(AES.decrypt(user.getPassword()));
		model.addAttribute("user",user);
		System.out.println("-----------------' "+clientInfo);*/
		return "redirect:registration?id="+user.getId();
	}
	
	@RequestMapping("/registration")
	public String registrationSuccess(Model model,@RequestParam long id) throws Exception {
		UserEntity user=userServices.findOne(new UserEntity(), id);
		user.setPassword(AES.decrypt(user.getPassword()));
		logger.info("new user "+user);
		model.addAttribute("user",user);
		return "registration_success";
		
	}
	
	@RequestMapping("/logout")
	public String userLogout(HttpSession session) {
		System.out.println("-------- USER LOGOUT----------"+session.getAttribute("user"));
		session.removeAttribute("user");
		
		return "redirect:/signin";
	}
}
