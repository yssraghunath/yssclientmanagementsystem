package com.yss.cms.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
public class UserEntity {
	
	@Id
	@GeneratedValue
	long id;
	
	@NotNull
	String name;
	
	@NotNull
	@Column(unique=true)
	String userName;

	@NotNull
	String password;
	

	String mobile;
	
	@NotNull
	@ManyToOne
	UserRoleEntity role;
	
	public UserEntity() {
		super();
		// TODO Auto-generated constructor stub
	}

	public UserEntity(long id, String name, String userName, String password, String mobile, UserRoleEntity role) {
		super();
		this.id = id;
		this.name = name;
		this.userName = userName;
		this.password = password;
		this.mobile = mobile;
		this.role = role;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public UserRoleEntity getRole() {
		return role;
	}

	public void setRole(UserRoleEntity role) {
		this.role = role;
	}

	@Override
	public String toString() {
		return "UserEntity [id=" + id + ", name=" + name + ", userName=" + userName + ", password=" + password
				+ ", mobile=" + mobile + ", role=" + role + "]";
	}

		
}
