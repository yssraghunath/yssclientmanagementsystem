<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

<script type="text/javascript">
	function loadPage() {

		var today = new Date();
		var dd = today.getDate();
		var mm = today.getMonth() + 1;
		var yyyy = today.getYear() + 1900;
		//find current date
		var currentDate = mm + "/" + dd + "/" + yyyy;
		console.log(" currentDate ", currentDate);

		var years = yyyy.toString();
		var year = years.slice(-2);
		year = +(year);
		//create invoice formate
		var yearFormat = year + "-" + (year + 1);
		console.log("YearFormat", yearFormat);
		var invoiceFormat = "YSS/" + yearFormat + "/" + "XX";
		console.log("year formate ", invoiceFormat);

		document.getElementById('invoiceNo').value = invoiceFormat;

		currentDate = yyyy + "-" + mm + "-" + dd;
		console.log(currentDate)
		var d = new Date(currentDate);
		console.log("ddd ", d)
		document.getElementById('invoiceDate').value = d;

	}

</script>


<title>Invoice Generate</title>
</head>
<body onLoad="loadPage()" class="container-fluid alert-success">

	<%-- <div class="row h1  justify-content-center" style="text-align: center;">
		<%@include file="headerpage.html" %>
	</div>
	
	

	<div class="row">
		<%@include file="menupage.html" %>
	</div>
 --%>
 <div>
		<%@include file="check_session.jsp" %>
</div>

	<div class="container">
		<div class="row" style="margin-top: 10px;">
			<div class="col-md-6 offset-md-3">
				<form name="invoiceForm" action="savebuyer" method="POST"	modelAttribute="invoiceDetails" onsubmit="return validation()">
					<div class="card">
						<div class="card-header">
							<h4>Buyer Details</h4>
						</div>
						<div class="card-body">
							<div class="form-group row">
								<label for="bill_to"
									class="col-sm-4 col-form-label col-form-label-sm">Bill
									To(Buyer) <span class="req" style="color: Red">*</span>
								</label>
								<div class="col-sm-6">
									<select class="form-control form-control-sm"
										name="buyerDetails">
										<option value="" disabled selected>Select your option</option>
										<c:forEach var="client" items="${clientInfo}">
											<option
												value="${client.companyName},${client.address},${client.gstNo}">${client.companyName}</option>
										</c:forEach>

									</select>
								</div>
							</div>
							<div class="form-group row">
								<label for="shipped_to"
									class="col-sm-4 col-form-label col-form-label-sm">Shipped
									To(Site) <span class="req" style="color: Red">*</span>
								</label>
								<div class="col-sm-6">
									<input type="text" class="form-control form-control-sm"
										name="siteDetails">
								</div>
							</div>
							<div class="form-group row" hidden>
								<label for="invoice_no"
									class="col-sm-4 col-form-label col-form-label-sm">Invoice
									No <span class="req" style="color: Red">*</span>
								</label>
								<div class="col-sm-6">
									<input type="text" class="form-control form-control-sm"
										name="invoiceNo" id="invoiceNo">
								</div>
							</div>
							<div class="form-group row">
								<label for="date"
									class="col-sm-4 col-form-label col-form-label-sm">Date
									<span class="req" style="color: Red">*</span>
								</label>
								<div class="col-sm-6">
									<input type="date" class="form-control form-control-sm"
										id="invoiceDate" name="invoiceDate">
								</div>
							</div>



							<div class="form-group row">
								<label for="due_date"
									class="col-sm-4 col-form-label col-form-label-sm">Due
									Date <span class="req" style="color: Red">*</span>
								</label>
								<div class="col-sm-6">
									<input type="text" class="form-control form-control-sm"
										name="dueDate">
								</div>
							</div>
							<div class="form-group row">
								<label for="quote"
									class="col-sm-4 col-form-label col-form-label-sm">Quote/ACC/Ref
									No <span class="req" style="color: Red">*</span>
								</label>
								<div class="col-sm-6">
									<input type="text" class="form-control form-control-sm"
										name="quoteRefNo">
								</div>
							</div>
							<div class="form-group row">
								<label for="client"
									class="col-sm-4 col-form-label col-form-label-sm">Client/PO/Ref
									No:- <span class="req" style="color: Red">*</span>
								</label>
								<div class="col-sm-6">
									<input type="text" class="form-control form-control-sm"
										name="clientPoNo">
								</div>
							</div>
							<div class="form-group row">
								<label for="po_so_date"
									class="col-sm-4 col-form-label col-form-label-sm">PO/SO
									Date <span class="req" style="color: Red">*</span>
								</label>
								<div class="col-sm-6">
									<input type="text" class="form-control form-control-sm"
										name="poDate">
								</div>
							</div>


							<div class="form-group row">
								<label for="duration_from"
									class="col-sm-4 col-form-label col-form-label-sm">AMC
									From <span class="req" style="color: Red">*</span>
								</label>
								<div class="col-sm-6">
									<input type="date" class="form-control form-control-sm"
										name="amcDateFrom">
								</div>
							</div>
							<div class="form-group row">
								<label for="duration_to"
									class="col-sm-4 col-form-label col-form-label-sm">To <span
									class="req" style="color: Red">*</span>
								</label>
								<div class="col-sm-6">
									<input type="date" class="form-control form-control-sm"
										name="amcDateTo">
								</div>
							</div>

							<div class="form-group row">
								<label for="payment"
									class="col-sm-4 col-form-label col-form-label-sm">Payment
									<span class="req" style="color: Red">*</span>
								</label>
								<div class="col-sm-6">
									<input type="text" class="form-control form-control-sm"
										name="paymentMode">
								</div>
							</div>
							<div class="form-group row">
								<label for="taxPayable"
									class="col-sm-4 col-form-label col-form-label-sm">Whether Tax Payable
									<span class="req" style="color: Red">*</span>
								</label>
								<div class="col-sm-6">
									<input type="text" class="form-control form-control-sm"
										name="taxPayable">
								</div>
							</div>
							<div class="form-group row" id="errorMessage" style="color:red;text-align:center;display:none">
								<p class="col-sm-6 offset-sm-3"> Please fill all information </p>
							</div>

							<div style="text-align: center;">
								<button type="submit" class="btn btn-primary">Next</button>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
	
	<script type="text/javascript">

	function validation() {
		
		var client=document.forms["invoiceForm"]["buyerDetails"].value;
		var shippedTO =document.forms["invoiceForm"]["siteDetails"].value;
	    var quoteRef =document.forms["invoiceForm"]["quoteRefNo"].value;
	    var clientRef =document.forms["invoiceForm"]["clientPoNo"].value;
	    var poDate=document.forms["invoiceForm"]["poDate"].value;
	    var amcFrom =document.forms["invoiceForm"]["amcDateFrom"].value;
	    var amcTo =document.forms["invoiceForm"]["amcDateTo"].value;
	    var payment =document.forms["invoiceForm"]["paymentMode"].value;
	    
	    if((shippedTO=="")||(quoteRef=="")||(clientRef=="")||(poDate=="")||(amcFrom=="")||(amcTo=="")||(payment=="")){
	    	
	    	document.getElementById("errorMessage").style.display = "block";
	    	 setTimeout(function () {
	    	        document.getElementById('errorMessage').style.display='none';
	    	    }, 5000);
	    	return false;
	    }else{
	    	
	    	return true;
	    }
		
	}
	
	</script>
</body>
</html>