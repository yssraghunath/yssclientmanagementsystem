$(document).ready( function () {
	 var table = $('#employeesTable').DataTable({
			"sAjaxSource": "/employees",
			"sAjaxDataProp": "",
			"order": [[ 0, "asc" ]],
			"aoColumns": [
			    { "mData": "id"},
		      { "mData": "itemName" },
				  { "mData": "itemCode" },
				  { "mData": "itemCodeV" },
				  { "mData": "itemCategory" },
				  { "mData": "itemMRP" }
			]
	 })
})

